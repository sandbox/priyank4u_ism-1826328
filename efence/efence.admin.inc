<?php
/**
 * efence Extension
 * @version 1.3
 * NOTICE OF LICENSE
 *
 * This code has been adopted from the reCAPTCHA module for drupal available at:
 * http://drupal.org/project/recaptcha
 * The original reCAPTCHA module was written by:
 * 	Rob Loach
 * @package efence Plugin Oct 11, 2012
 * @author Priyank
 * @authorEmail efence.support@engageclick.com
 * @copyright (C) 2012 by EngageClick.com
 * @url http://efence.engageclick.com
 * @license http://www.gnu.org/copyleft/gpl.html GNU/GPL
**/

/**
 * @file
 * Provides the efence administration settings.
 */

/**
 * Form callback; administrative settings for efence.
 */
function efence_admin_settings() {
  // Load the efence library.
  _efence_load_library();

  $form = array();
  $form['efence_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public Key'),
    '#default_value' => variable_get('efence_public_key', ''),
    '#maxlength' => 500,
    '#description' => t('The public key given to you when you <a href="@url" target="_blank">registered at efence.net</a>.', array('@url' => url(adcaptcha_get_signup_url($_SERVER['SERVER_NAME'], variable_get('site_name', ''))))),
    '#required' => TRUE,
  );
  $form['efence_private_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Private Key'),
    '#default_value' => variable_get('efence_private_key', ''),
    '#maxlength' => 500,
    '#description' => t('The private key given to you when you <a href="@url" target="_blank">registered at efence.net</a>.', array('@url' => url(adcaptcha_get_signup_url($_SERVER['SERVER_NAME'], variable_get('site_name', ''))))),
    '#required' => TRUE,
  );
  $form['efence_zone_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Zone Id'),
	'#maxlength' => 5,
    '#default_value' => variable_get('efence_zone_id', '2'),
    '#description' => t('Zone Id for your ad preference in efence'),
	'#required' => TRUE,
  );
  $form['efence_theme'] = array(
    '#type' => 'radios',
    '#title' => t('Theme'),
    '#description' => t('Theme for efence captcha'),
    '#options' => array(
      'red' => t('Red'),
      'blue' => t('Blue'),
	  'grey' => t('Grey'),
      'black' => t('Black'),
    ),
    '#default_value' => variable_get('efence_theme', 'grey'),
  );
  
  $form['efence_hide_for_loggedin_user'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide for Logged in User?'),
    '#default_value' => variable_get('efence_hide_for_loggedin_user', FALSE),
    '#description' => t('Hide efence AdCaptcha for logged in User?'),
  );
  return system_settings_form($form);
}

/**
 * Validation function for efence_admin_settings().
 *
 * @see efence_admin_settings()
 */
function efence_admin_settings_validate($form, &$form_state) {
  /*$tabindex = $form_state['values']['efence_tabindex'];
  if (!empty($tabindex) && !is_numeric($tabindex)) {
    form_set_error('efence_tabindex', t('The Tab Index must be an integer.'));
  }*/
}
