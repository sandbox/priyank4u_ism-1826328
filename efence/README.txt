
efence for Drupal
====================

The efence module uses the efence web service to
improve the CAPTCHA system and protect email addresses. For
more information on what efence is, please visit:
    http://efence.engageclick.com


INSTALLATION
------------

1. Extract the efence module to your local favourite
   modules directory (sites/all/modules).


CONFIGURATION
-------------
   
1. Enable efence and CAPTCHA modules in:
       admin/build/modules

2. You'll now find a efence tab in the CAPTCHA
   administration page available at:
       admin/config/people/captcha/efence

3. Register for a public & private efence key and zone id at:
       http://efence.engageclick.com

4. Input the keys,zone id into the efence settings. The rest of
   the settings should be fine as their defaults.

5. Visit the Captcha administration page and set where you
   want the efence form to be presented:
       admin/user/captcha

CUSTOM efence THEME
----------------------

You can create a custom efence theme widget by setting
the theme of the efence form to "custom" in the
efence administration page.  This will output a custom
form that is themeable through the theme function:
  theme_efence_custom_widget().

If you don't implement this function, it is still quite
easily customizable through manipulating the CSS.

For more information on this, visit:
http://efence.engageclick.com/


CHANGELOG
---------

http://drupal.org/project/cvs/
