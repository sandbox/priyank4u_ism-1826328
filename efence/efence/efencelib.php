<?php
/**
 * efence Extension
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * This code has been adopted from the reCAPTCHA module available at:
 * http://www.google.com/recaptcha
 * The original reCAPTCHA module was written by:
 * 	Mike Crawford
 * 	Ben Maurer
 *
 * @category   efence
 * @package    efence
 * @author     Basant Choudhary
 * @authorEmail efence.support@engageclick.com
 * @copyright  Copyright (c) 2012 Engageclick (http://www.efence.engageclick.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

 	//EngageClick Server address used for generation and validation of AdCaptcha
    define("ECAD_CAPTCHA_API_SERVER_HOST", "efence.engageclick.com");
    define("ECAD_CAPTCHA_API_SECURE_SERVER","https://efence.engageclick.com/api");
	define("ECAD_VERIFY_SERVER", "www.google.com");

	/**
 	 * Encodes the given data into a query string format
 	 * @param $data - array of string elements to be encoded
 	 * @return string - encoded request
 	 **/
	function qencode ($data) {
        $req = "";
        foreach ( $data as $key => $value )
    		$req .= $key . '=' . urlencode( stripslashes($value) ) . '&';

        // Cut the last '&'
        $req=substr($req,0,strlen($req)-1);
        return $req;
	}

	/**
 	 * Validates the response for captcha
 	 * @param $chid - array of string elements to be encoded
	 * @param $response - response to a captcha puzzle
	 * @param $asid - ASID
	 * @param $remoteip - Ip Address of client
	 * @param $privatekey - EngageClick private key of publisher
 	 * @return validationResponse - Validation Response 
 	**/
	function captcha_check_answer($chid, $response, $asid, $remoteip, $privatekey, $extra_params = array()) {
			//Validate if public and private keys are null
			if ($privatekey == null || $privatekey == '') {
				die ("you must get an API key ");
			}			
			
			//Mandatory field validation for remote ip
			if ($remoteip == null || $remoteip == '') {
				die ("you must pass the remote ip to ECad Server");
			}

	        //discard spam submissions
	        if ($chid == null || strlen($chid) == 0 || $response == null || strlen($response) == 0) {
	                $recaptcha_response = array(); //new ValidationResponse();
	                $recaptcha_response["is_valid"] = false;
	                $recaptcha_response["error"] = 'request-not-sent';
	                return $recaptcha_response;
	        }

			$data = array ( 'privatekey' => $privatekey,
			'remoteip' => $remoteip,
           	'chid' => $chid,
			'adlogid'=> $asid,
			'input' => $response
            );
			
			//Send request to EngageClick server for validating response
			$response = ecadhttppost (ECAD_CAPTCHA_API_SERVER_HOST,
	         		"/validate", $data);
            $answers = explode ("\n", $response [1]);
    	    $recaptcha_response = array(); //new ValidationResponse();
        	if (trim ($answers [0]) == 'true') {
            	    $recaptcha_response["is_valid"] = true;
        	}
        	else {
            	    $recaptcha_response["is_valid"] = false;
                	$recaptcha_response["error"] = $answers [1];
        	}
        	return $recaptcha_response;
	}

	/**
 	 * Wrapper function for making HTTP calls
 	 * @param $url - URL for HTTP request
	 * @param $get - parameters to be sent as query string in HTTP request
	 * @param $options - request options
 	 * @return http response: response for http request
 	**/
	function curl_get($url, array $get = NULL, array $options = array())
	{
		$defaults = array(
			CURLOPT_URL => $url. (strpos($url, '?') === FALSE ? '?' : ''). http_build_query($get),
			CURLOPT_HEADER => 0,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_TIMEOUT => 30
		);

		$ch = curl_init();
		curl_setopt_array($ch, ($options + $defaults));
		if( ! $result = curl_exec($ch))
		curl_close($ch);
		return $result;
	}

    function RedirectToURL($url)
    {
        header("Location: $url");
        exit;
    }

	/**
 	 * Wrapper function for making HTTP calls
 	 * @param $host - Address of EngageClick server
	 * @param $port - Port to connect
	 * @param $path - path for the api
	 * @param $data - query string for http request
 	 * @return http response: response for http request
 	**/
	function ecadhttppost ($host, $path, $data, $port = 80) {
        $req = qencode ($data);

        $http_request  = "POST $path HTTP/1.0\r\n";
        $http_request .= "Host: $host\r\n";
        $http_request .= "Content-Type: application/x-www-form-urlencoded;\r\n";
        $http_request .= "Content-Length: " . strlen($req) . "\r\n";
        $http_request .= "User-Agent: ECadLIB/PHP\r\n";
        $http_request .= "\r\n";
        $http_request .= $req;
        $response = '';
        if( false == ( $fs = @fsockopen($host, $port, $errno, $errstr, 10) ) ) {
                die ('Could not open socket');
        }

        fwrite($fs, $http_request);

        while ( !feof($fs) )
                $response .= fgets($fs, 1160); // One TCP-IP packet
        fclose($fs);
        $response = explode("\r\n\r\n", $response, 2);
        return $response;
	}

	function ipCheck() {
		if (getenv('HTTP_CLIENT_IP')) {
			$ip = getenv('HTTP_CLIENT_IP');
		}
		elseif (getenv('HTTP_X_FORWARDED_FOR')) {
			$ip = getenv('HTTP_X_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_X_FORWARDED')) {
			$ip = getenv('HTTP_X_FORWARDED');
		}
		elseif (getenv('HTTP_FORWARDED_FOR')) {
			$ip = getenv('HTTP_FORWARDED_FOR');
		}
		elseif (getenv('HTTP_FORWARDED')) {
			$ip = getenv('HTTP_FORWARDED');
		}
		else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}


    /**
     * Gets the challenge HTML (javascript and non-javascript version).
     * This is called from the browser, and the resulting EngageClick HTML widget
     * is embedded within the HTML form it was called from.
     * @param string $pubkey A public key for EngageClick
     * @param string $error The error given by EngageClick (optional, default is null)
     * @param boolean $use_ssl Should the request be made over ssl? (optional, default is false)

     * @return string - The HTML to be embedded in the user's form.
     */
    function adcaptcha_get_html ($pubkey, $zoneId, $theme, $error = null, $use_ssl = false)
    {
	    if ($pubkey == null || $pubkey == '') {
		    die ("To use adCAPTCHA you must get an API key from <a href='http://efence.engageclick.com'>efence.engageclick.com</a>");
	    }

	    if ($use_ssl) {
            $server = ECAD_CAPTCHA_API_SECURE_SERVER;
        } else {
	     	$server = 'http://' . ECAD_CAPTCHA_API_SERVER_HOST . "/api";
        }

        $errorpart = "";
        if ($error) {
           $errorpart = "&amp;error=" . $error;
        }

        //Check NoScript part
        return '<script type= "text/javascript"> var ECPuzzleConfig = { theme : "' . $theme . '"}; </script>
		<script type="text/javascript" src="'. $server . '?pubkey=' . $pubkey . '&zoneid=' . $zoneId . $errorpart . '"></script>

	    <noscript>
  		    <iframe src="'. $server . '/noscript?k=' . $pubkey . $errorpart . '" height="300" width="500" frameborder="0"></iframe><br/>
  		    <textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
  		    <input type="hidden" name="recaptcha_response_field" value="manual_challenge"/>
	    </noscript>';
    }
	
	function adcaptcha_get_signup_url ($domain = null, $appname = null) {
		return "https://efence.engageclick.com";
	}
?>
